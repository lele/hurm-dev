# -*- coding: utf-8 -*-
# :Project:   hurm -- Master makefile
# :Created:   mer 27 gen 2016 17:18:13 CET
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2016 Lele Gaifax
#

export TOPDIR := $(CURDIR)
export VENVDIR := $(TOPDIR)/env/
export BINDIR := $(VENVDIR)bin/
export PYTHON := $(BINDIR)python
export SHELL := /bin/bash
export SYS_PYTHON ?= /usr/bin/python3

all: help

include Makefile.db
include Makefile.fe
include Makefile.virtualenv
