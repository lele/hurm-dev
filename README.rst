.. -*- coding: utf-8 -*-
.. :Project:   hurm -- Human Resources Manager
.. :Created:   lun 08 feb 2016 11:36:19 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2016 Lele Gaifax
..

=========================
 Human Resources Manager
=========================

Development glue
================

 :author: Lele Gaifax
 :contact: lele@metapensiero.it
 :license: GNU General Public License version 3 or later

This is the top level project that glues together the various pieces that compose the HuRM
application, targeting its development.


First steps
-----------

1. Clone this repository and its submodules with something like

  ::

    $ git clone --recursive https://bitbucket.org/lele/hurm-dev.git hurm

2. Setup development mode: this will create a Python 3 virtual environment and install the
   required source of ExtJS 4

  ::

    $ cd hurm
    $ make development

3. Initialize the ``hurm`` PostgreSQL database

  ::

    $ cd hurm
    $ source env/bin/activate
    $ make database0

4. Load some data, either test or historical

  ::

    $ make load-test-data


Development
-----------

You can run the development application with

::

  $ make run-development

and open ``http://localhost:6543/`` in a browser: if you loaded the test data, you can log in
using ``bob@example.com`` with password ``test`` as credentials.

Unit and functional tests can be executed with

::

  $ make test-database test-frontend
